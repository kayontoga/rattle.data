APP=rattle.data
VER=1.0.2

DATE=$(shell date +%Y-%m-%d)
FILES=Makefile DESCRIPTION data inst man
RSITELIB=$(shell Rscript -e "cat(installed.packages()['$(APP)','LibPath'])")

######################################################################
# R Package Management

.PHONY: version
version:
	perl -pi -e 's|^Version: .*|Version: $(VER)|' DESCRIPTION
	perl -pi -e 's|^Date: .*|Date: $(DATE)|' DESCRIPTION

.PHONY: check
check: clean version build
	/usr/lib/R/bin/R CMD check --as-cran --check-subdirs=yes $(APP)_$(VER).tar.gz

.PHONY: build
build: version $(APP)_$(VER).tar.gz

.PHONY: rebuild
rebuild: build install

.PHONY: install
install: build
	/usr/lib/R/bin/R CMD INSTALL $(APP)_$(VER).tar.gz

.PHONY: test
test: install
	Rscript -e 'library($(APP));$(APP)();Sys.sleep(120)'

$(APP)_$(VER).tar.gz: $(FILES)
	/usr/lib/R/bin/R CMD build .

$(APP)_$(VER).check: $(APP)_$(VER).tar.gz
	/usr/lib/R/bin/R CMD check --as-cran --check-subdirs=yes $^

.PHONY: ucheck
ucheck: $(APP)_$(VER).tar.gz
	sh ./upload_uwe.sh
	@echo Wait for email from Uwe Legge.

.PHONY: cran
cran: $(APP)_$(VER).tar.gz
	sh ./upload_cran.sh
	@echo Be sure to email cran@r-project.org.

.PHONY: src
src: $(APP)_$(VER)_src.zip  $(APP)_$(VER)_src.tar.gz

.PHONY: zip
zip: $(APP)_$(VER).zip

$(APP)_$(VER)_src.zip: Makefile $(FILES)
	zip -r $@ $^
		-x *~		 \
		-x */*~          \
		-x .Rbuildignore \
		-x .git/**\* 	 \
		-x .git/      	 \
		-x .gitignore 	 \
		-x .Rhistory

$(APP)_$(VER)_src.tar.gz: Makefile $(FILES)
	tar zcvf $@ $^ \
		--exclude="*~" 		\
		--exclude=".git*" 	\
		--exclude=".R*"

$(APP)_$(VER).zip: install
	(cd $(RSITELIB); zip -r9 - $(APP)) >| $(APP)_$(VER).zip

.PHONY: show
show: $(APP)_$(VER).tar.gz
	tar tvf $^
	ls -la $^

########################################################################
# Version Control - git

status:
	@echo "-------------------------------------------------------"
	git status --untracked-files=no
	@echo "-------------------------------------------------------"

info:
	git info

pull:
	@echo "-------------------------------------------------------"
	git pull
	@echo "-------------------------------------------------------"

push:
	@echo "-------------------------------------------------------"
	git push
	@echo "-------------------------------------------------------"

diff:
	@echo "-------------------------------------------------------"
	git --no-pager diff --color
	@echo "-------------------------------------------------------"

difftool:
	git difftool

log:
	@echo "-------------------------------------------------------"
	git --no-pager log --stat --max-count=10
	@echo "-------------------------------------------------------"

fulllog:
	@echo "-------------------------------------------------------"
	git --no-pager log
	@echo "-------------------------------------------------------"

########################################################################
# Administration

.PHONY: clean
clean:
	rm -vf *~ .*~ */*~
	rm -rf $(APP).Rcheck

.PHONY: realclean
realclean: clean
	-@mv -v $(APP)_* BACKUP/
